#!/usr/bin/env python


def json_decode(json_string: str) -> dict:
    """Decode a JSON string into a workable, indexed dictionary

    Args:
        json_string (str): JSON string

    Returns:
        dict: dictionary object parsed from JSON string
    """
    import json
    return json.loads(json_string)


def json_encode(dict_obj: dict, print_pretty: bool = False, sort: bool = False) -> str:
    """Encode a JSON string from a dictionary

    Args:
        dict_obj (dict): dictionary object to be stringified
        
        print_pretty (bool): whether or not to print the JSON string in a more readable format. Default: False
        
        sort (bool): Whether or not to sort the keys of the JSON string. Default: False

    Returns:
        str: JSON string
    """
    import json
    if print_pretty == True:
        return json.dumps(dict_obj, indent=4, sort_keys=sort)
    else:
        return json.dumps(dict_obj, sort_keys=sort)


def validate_timestamp(timestamp: str) -> bool:
    """Validate that a timestamp is formatted correctly

    Args:
        timestamp (str): timestamp to be tested

    Returns:
        bool: True if valid
    """
    import datetime
    try:
        datetime.datetime.strptime(timestamp, "%H:%M:%S")
    except ValueError:
        return False
    return True


def file_exists(path: str) -> bool:
    """Determine if a file exists.

    Args:
        path (str): Path to the file to test.

    Returns:
        bool: True if file exists.
    """
    try:
        open(path, "r")
    except FileNotFoundError:
        return False
    return True
