class JSONTools():

    def __init__(self) -> None:
        from os import getcwd, path
        self.working_path = getcwd() + path.sep
        self.sample_json = self.working_path + 'sample.json'


    def generate_sample_json(self) -> None:
        """Generate a sample JSON file which can be used to parsed into the app.

        The file 'sample.json will be save in the current working directory.
        """
        from lib.functions import json_encode
        data = {
            'file': 'PATH_TO_ORIGONAL_FILE',
            'output': 'mp3',
            'parts': {
                'NEW_FILE_NAME': {
                    'start_time': 'HH:MM:SS',
                    'end_time': 'HH:MM:SS'
                }
            }
        }
        file = open(self.sample_json, "w")
        file.write(json_encode(data, True))
        file.close
        print('File created: ' + self.sample_json)


    def parse_json(self, file: str) -> dict:
        """Parse a JSON file into a dictionary.

        Args:
            file (str): path to the JSON file to decode.

        Returns:
            dict: decoded data.
        """
        try:
            json_text = open(file, "r")
        except FileNotFoundError:
            print("JSON file " + file + " does not exist. Please indicate the correct file.")
            exit()
        from lib.functions import json_decode
        return json_decode(json_text.read())
