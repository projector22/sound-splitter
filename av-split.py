#!/usr/bin/env python

import argparse

ap = argparse.ArgumentParser()
ap.add_argument(
    "-g", "--generate-sample", 
    required=False, 
    action='store_true', 
    help="generate a example or sample JSON file which can be used as a template when running the app."
)
ap.add_argument(
    "-p", "--json-path",
    required=False,
    help="the path of the JSON file used in the execution of AV splitting. Required in the usual run of things."
)
ap.add_argument(
    "-v", "--verbose",
    required=False,
    action="store_true",
    help="show all the FFMpeg feedback text."
)
ap.add_argument(
    "-o", "--overwrite-existing",
    required=False,
    action="store_true",
    help="cause the app to overwrite an existing file of the same name."
)
arg = vars(ap.parse_args())

from lib.JSONTools import JSONTools
json = JSONTools()

if arg['generate_sample'] == True:
    json.generate_sample_json()
elif arg['json_path'] != None:
    from lib.AVSplit import AVSplit
    splitter = AVSplit(
        json.parse_json(arg['json_path']), {
            'verbose': arg['verbose'], 
            'overwrite': arg['overwrite_existing']
        }
    )
    splitter.split()
else:
    print("No instruction of file selected. Terminating. Please parse -h to see available options.")
